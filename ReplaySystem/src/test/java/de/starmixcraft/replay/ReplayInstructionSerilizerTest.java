package de.starmixcraft.replay;

import java.util.Arrays;

import org.junit.Test;

import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;

public class ReplayInstructionSerilizerTest {

	
	
	@Test
	public void ReplayInstructionSerilizer0() {
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
		instructionSerilizer.writeVarIntArray(new int[] {1,2,4,5,6,7,8,9});
		instructionSerilizer.writeByteArray(new byte[] {1,12,3,32,1,32,2,4});
		instructionSerilizer.writeCharArray(new char[] {213,2343,234,123,23445});
		System.out.println(Arrays.toString(instructionSerilizer.readVarIntArray()));
		System.out.println(Arrays.toString(instructionSerilizer.readByteArray()));
		System.out.println(Arrays.toString(instructionSerilizer.readCharArray()));
	}
}
