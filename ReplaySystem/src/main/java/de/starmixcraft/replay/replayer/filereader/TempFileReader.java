package de.starmixcraft.replay.replayer.filereader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.replayer.ReplayInfo;
import de.starmixcraft.replay.utils.InstructionUtils;
import de.starmixcraft.replay.utils.StreamUtils;
import io.netty.buffer.ByteBufAllocator;

@Deprecated
public class TempFileReader implements Runnable, ReplayFileReader{

	private FileInputStream scanner;
	private ArrayList<ReplayInstruction> bufferdInstuctions;
	private Thread thread;
	public TempFileReader(File tempfile)  {
	
		try {
			scanner = new FileInputStream(tempfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		thread = new Thread(this);
	}
	
	public void startReader() {
		thread.start();
	}
	
	public ReplayInfo readInfo() {
		ReplayInfo info = new ReplayInfo();
		byte[] data = readLine();
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer(ByteBufAllocator.DEFAULT.buffer(data.length));
		instructionSerilizer.writeBytes(data);
		info.readFromSerelizer(instructionSerilizer);
		startReader();
		return info;
	}
	
	
	public ArrayList<ReplayInstruction> getNextInstuctionSet(){
		if(bufferdInstuctions == null) {
			synchronized (this) {
				try {
					wait(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		ArrayList<ReplayInstruction> instructions = bufferdInstuctions;
		bufferdInstuctions = null;
		bufferNextSet();
		return instructions;
	}
	
	public void close() {
		thread.interrupt();
		try {
			scanner.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void bufferNextSet() {
		synchronized (thread) {
			thread.notifyAll();
		}
	}
	
	public byte[] readLine() {
		try {
			return readWithLength();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private byte[] readWithLength() throws IOException {
		int length = StreamUtils.readIntFromStream(scanner);
		byte[] data = new byte[length];
		scanner.read(data);
		return data;
	}

	@Override
	public void run() {
		while (true) {
			byte[] data = readLine();
			ArrayList<ReplayInstruction> arrayList = InstructionUtils.bytesToList(data);
			bufferdInstuctions = arrayList;
			synchronized (this) {
				notifyAll();
			}
			synchronized (thread) {
				try {
					thread.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}



	@Override
	public boolean hasNextInstuctionSet() {
		return true;
	}
	
	
	
}
