package de.starmixcraft.replay.replayer.filereader;

import java.util.ArrayList;

import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.replayer.ReplayInfo;

public interface ReplayFileReader {

	
	
	public ArrayList<ReplayInstruction> getNextInstuctionSet();
	public void close();
	public byte[] readLine();
	public boolean hasNextInstuctionSet();
	public ReplayInfo readInfo();
}
