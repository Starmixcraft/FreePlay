package de.starmixcraft.replay.replayer;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.entity.Player;

import de.starmixcraft.replay.ReplayAdapter;
import de.starmixcraft.replay.backend.Backend;
import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.replayer.filereader.ReplayFileReader;
import de.starmixcraft.replay.replayer.filereader.TempFileReader;
import de.starmixcraft.replay.utils.TickIndependentTimer;
import de.starmixcraft.replay.versions.ServerVersion;
import de.starmixcraft.replay.world.World;
import lombok.Getter;
import lombok.Setter;

public class Replay implements Runnable{

	
	private InstructionNotifiactionBus instructionNotifiactionBus;
	@Getter
	private Player[] player;
	private ReplayFileReader tempFileReader;
	@Getter
	private ReplayInfo replayInfo;
	private TickIndependentTimer tickIndependentTimer;
	@Getter
	private int currenttick;
	@Getter
	@Setter
	private World world;
	public Replay(Player[] player, String replayid, Backend backend, ServerVersion serverVersion, File tempfile) {
		this.player = player;
		this.tempFileReader = new ReplayLoader(replayid, backend, tempfile).loadWithoutChecks();
		this.replayInfo = tempFileReader.readInfo();	
		this.instructionNotifiactionBus = ReplayAdapter.getInstructionNotifiactionBus();
	}
	
	
	public void startReplay() {
		ReplayManager.getInstance().startReplay(this);
		tickIndependentTimer = new TickIndependentTimer(this);
	}

	public void stopReplay() {
		tickIndependentTimer.stop();
		ReplayManager.getInstance().stopReplay(this);
	}
	
	
	
	@Override
	public void run() {
		if(!tempFileReader.hasNextInstuctionSet()) {
			brodcast("End of replay reached!");
			tickIndependentTimer.stop();
			return;
		}
		try {
		ArrayList<ReplayInstruction> instructions = tempFileReader.getNextInstuctionSet();
		for(ReplayInstruction instruction : instructions) {
			instructionNotifiactionBus.execute(this, instruction);
		}
		world.tick(player);
		if(instructions.size()>0)
		System.out.println("Processed " + instructions.size() + " instructions in tick " + currenttick);
		currenttick++;
		}catch (Exception e) {
			tickIndependentTimer.stop();
			brodcast("Exeption whiles replaying replay see console for more details!");
			e.printStackTrace();
		}
	}
	
	
	public void brodcast(String message) {
		for(Player p : player) {
			p.sendMessage(message);
		}
	}
}
