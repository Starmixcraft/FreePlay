package de.starmixcraft.replay.replayer;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.Cipher;

import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class InstructionNotifiactionBus {
	private HashMap<Instruction, InstructionHandler> executors;
	private ReentrantLock lock = new ReentrantLock();

	public InstructionNotifiactionBus() {
		executors = new HashMap<>();
	}
	
	
	
	
	public void execute(Replay replay, ReplayInstruction instruction) {
		InstructionHandler instructionHandler = executors.get(instruction.getInstruction());
		if(instructionHandler == null) {
			throw new Error("Cant execute instruction " + instruction.getInstruction().toString() + " no executor found!");
		}
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
		instructionSerilizer.writeBytes(instruction.getData());
		try {
			instructionHandler.getMethod().invoke(instructionHandler.getInstance(), new Object[] {replay, instructionSerilizer});
		} catch (IllegalAccessException e) {
			System.out.println("Method " + instructionHandler.getMethod().getName() + " became unaccessebel");
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println("Method " + instructionHandler.getMethod().getName() + " now wants more then 1 argument");
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			System.out.println("Method " + instructionHandler.getMethod().getName() + " invocation error");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Method " + instructionHandler.getMethod().getName() + " invocation error");
			e.printStackTrace();
		}
	}
	
	
	public void registerHandlers(Object object) {
		lock.lock();
		try {
		Method[] methods = object.getClass().getDeclaredMethods();
		for(Method method : methods) {
			InstructionExecutor instructionExecutor = method.getAnnotation(InstructionExecutor.class);
			if(instructionExecutor == null) {
				System.out.println("Method with name " + method.getName() + " dosent have the  InstructionExecutor");
				continue;
			}
			if(method.getParameterCount() != 2) {
				System.out.println("Warning the method " + method.getName() + " dosent have 2 parameter!");
				continue;
			}
			executors.put(instructionExecutor.instruction(), new InstructionHandler(object, method));
		}
		}finally {
			lock.unlock();	
		}
	}
	
	
	@AllArgsConstructor
	@Getter
	private static class InstructionHandler{
		private Object instance;
		private Method method;
	}
}
