package de.starmixcraft.replay.replayer.filereader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

public class FileUncompressor {

	private FileInputStream inputStream;
	
	private FileOutputStream outputStream;
	
	public FileUncompressor(File in, File out)  {
		try {
		inputStream = new FileInputStream(in);
		outputStream = new FileOutputStream(out);
		}catch (Exception e) {
			e.printStackTrace();
			throw new Error(e);
		}
	}
	
	
	
	public void uncompress()  {
		try {
		BZip2CompressorInputStream compressorInputStream = new BZip2CompressorInputStream(inputStream, true);
		byte[] buf = new byte[1024];
		while (compressorInputStream.read(buf) > 0) {
			outputStream.write(buf);
			outputStream.flush();
		}
		compressorInputStream.close();
		inputStream.close();
		outputStream.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw new Error(e);
		}
	}
}
