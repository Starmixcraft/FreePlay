package de.starmixcraft.replay.replayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.bukkit.block.Beacon;

import de.starmixcraft.replay.backend.Backend;
import de.starmixcraft.replay.replayer.filereader.BasicReplayFileReader;
import de.starmixcraft.replay.replayer.filereader.FileUncompressor;
import de.starmixcraft.replay.replayer.filereader.ReplayFileReader;
import de.starmixcraft.replay.replayer.filereader.TempFileReader;

public class ReplayLoader {

	
	private String id;
	private Backend backend;
	private File tempfile;
	
	public ReplayLoader(String id, Backend backend, File tempfile) {
		this.id = id;
		this.backend = backend;
		this.tempfile = tempfile;
	}
	
	

	
	public ReplayFileReader loadWithoutChecks() {
		try {
			File compfile = new File(tempfile.getName() + ".comp");
			FileOutputStream fileOutputStream = new FileOutputStream(compfile);
			fileOutputStream.write(backend.loadReplay(id));
			fileOutputStream.close();
			FileUncompressor fileUncompressor = new FileUncompressor(compfile, tempfile);
			fileUncompressor.uncompress();
			ReplayFileReader fileReader = new BasicReplayFileReader(tempfile);
			return fileReader;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
