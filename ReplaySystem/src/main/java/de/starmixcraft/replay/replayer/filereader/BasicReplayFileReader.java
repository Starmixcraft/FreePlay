package de.starmixcraft.replay.replayer.filereader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.replayer.ReplayInfo;
import de.starmixcraft.replay.utils.InstructionUtils;
import de.starmixcraft.replay.utils.StreamUtils;
import io.netty.buffer.ByteBufAllocator;

public class BasicReplayFileReader implements ReplayFileReader {

	private FileInputStream fileInputStream;
	private long filebytes;
	private long readbytes;
	public BasicReplayFileReader(File file) {
		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ArrayList<ReplayInstruction> getNextInstuctionSet() {
		byte[] data = readLine();
		return InstructionUtils.bytesToList(data);
	}

	@Override
	public void close() {
		try {
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public byte[] readLine() {
		try {
			int length;
			length = StreamUtils.readIntFromStream(fileInputStream);
			byte[] data = new byte[length];
			fileInputStream.read(data);
			readbytes += 4; //Int
			readbytes += data.length; //The data
			return data;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean hasNextInstuctionSet() {
		// TODO Auto-generated method stub
		return filebytes > readbytes;
	}

	@Override
	public ReplayInfo readInfo() {
		ReplayInfo info = new ReplayInfo();
		byte[] data = readLine();
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer(ByteBufAllocator.DEFAULT.buffer(data.length));
		instructionSerilizer.writeBytes(data);
		info.readFromSerelizer(instructionSerilizer);
		try {
			filebytes = StreamUtils.readLongFromStream(fileInputStream);
			System.out.println("Bytes in this replay are" + filebytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return info;
	}

}
