package de.starmixcraft.replay.replayer;

import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.versions.ServerVersion;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ReplayInfo {
	private int lengthInTicks;
	private ServerVersion replayVersion;

	public void writeToSerelizer(ReplayInstructionSerilizer serilizer) {
		serilizer.writeVarInt(lengthInTicks);
		serilizer.writeEnum(replayVersion);
	}

	public void readFromSerelizer(ReplayInstructionSerilizer serilizer) {
		lengthInTicks = serilizer.readVarInt();
		replayVersion = serilizer.readEnum(ServerVersion.class);
	}

}
