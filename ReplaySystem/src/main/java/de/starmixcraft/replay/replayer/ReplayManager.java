package de.starmixcraft.replay.replayer;

import java.util.HashMap;

import org.bukkit.entity.Player;

import lombok.Getter;

public class ReplayManager {
	private HashMap<Player, Replay> replays;
	@Getter
	private static ReplayManager instance;
	public ReplayManager() {
		replays = new HashMap<>();
		instance = this;
	}
	
	
	public Replay getReplay(Player p) {
		return replays.get(p);
	}
	
	
	public void startReplay(Replay replay){
		for(Player p : replay.getPlayer()) {
			replays.put(p, replay);
		}
	}
	
	public void stopReplay(Replay replay) {
		for(Player p : replay.getPlayer()) {
			replays.remove(p);
		}
	}

}
