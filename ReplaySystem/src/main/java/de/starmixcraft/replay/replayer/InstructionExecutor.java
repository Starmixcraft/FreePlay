package de.starmixcraft.replay.replayer;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.starmixcraft.replay.instructions.Instruction;

@Retention(RetentionPolicy.RUNTIME)
@Target(METHOD)
public @interface InstructionExecutor {
	public Instruction instruction();
}
