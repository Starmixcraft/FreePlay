package de.starmixcraft.replay.backend;

public interface Backend {

	
	public String[] getReplays(int max);
	public byte[] loadReplay(String replayid);
	public boolean isReplayIDFree(String id);
	public String generateReplayID();
	public void claimReplayID(String id);
	public void saveReplay(String id, byte[] data);
	public String getFreeReplayID();
}
