package de.starmixcraft.replay.backend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import io.netty.util.internal.ThreadLocalRandom;

public class FileBackend implements Backend{
	private static final String fileending = ".rec";
	private static final String apfhabet = "abcdefghijklmnopqrstuvwxyz";
	private static final char[] idchars = ("012345679" + apfhabet + apfhabet.toUpperCase()).toCharArray();
	private File replayfolder;
	public FileBackend(File parent) {
		replayfolder = parent;
		if(!parent.exists()) {
			parent.mkdirs();
		}
	}
	@Override
	public String[] getReplays(int max) {
		File[] replayfiles = replayfolder.listFiles();
		String[] replays = new String[max];
		int i = 0;
		for(File file : replayfiles) {
			if(file.isDirectory()) {
				continue;
			}
			if(i+1 >= max) {
				break;
			}
			replays[i] = file.getName();
		}
		return replays;
	}

	@Override
	public byte[] loadReplay(String replayid) {
		File replayfile = getFile(replayid);
		byte[] data = new byte[(int) replayfile.length()];
		try {
			FileInputStream fileInputStream = new FileInputStream(replayfile);
			fileInputStream.read(data);
			fileInputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public boolean isReplayIDFree(String id) {
		return !getFile(id).exists();
	}

	@Override
	public String generateReplayID() {
		String id = "";
		for (int i = 0; i < 10; i++) {
			id += idchars[ThreadLocalRandom.current().nextInt(idchars.length)];
		}
		return id;
	}

	@Override
	public void claimReplayID(String id) {
		File file = getFile(id);
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void saveReplay(String id, byte[] data) {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(getFile(id));
			fileOutputStream.write(data);
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	private File getFile(String id) {
		return new File(replayfolder, id + fileending);
	}
	@Override
	public String getFreeReplayID() {
		String id = generateReplayID();
		if(isReplayIDFree(id)) {
			return id;
		}else {
			return getFreeReplayID();
		}
	}
}
