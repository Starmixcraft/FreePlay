package de.starmixcraft.replay;

import de.starmixcraft.replay.recording.Record;
import de.starmixcraft.replay.replayer.InstructionNotifiactionBus;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replay.replayer.ReplayManager;
import de.starmixcraft.replay.versions.ServerVersion;
import lombok.Getter;

public abstract class ReplayAdapter {
	@Getter
	private String versionString;
	@Getter
	private static InstructionNotifiactionBus instructionNotifiactionBus;
	@Getter
	private ServerVersion serverVersion;

	public ReplayAdapter(String versionString, ServerVersion serverVersion) {
		this.versionString = versionString;
		instructionNotifiactionBus = new InstructionNotifiactionBus();
		this.serverVersion = serverVersion;
	}

	public abstract void initRecordSystem();

	public void initReplaySystem() {
		 new ReplayManager();
		initReplaySystem0();
	}

	public abstract void initReplaySystem0();

	public abstract void initReplay(Replay replay);

	public abstract void startRecording(Record record);

	public void registerInstructionExecutors(Object object) {
		instructionNotifiactionBus.registerHandlers(object);
	}
}
