package de.starmixcraft.replay.instructions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BufferdInstructionMap extends HashMap<Integer, Object>{

	private static final long serialVersionUID = 3513588435621086118L;
	private boolean lockadd;
	private Object lockaddobjekt;
	private boolean currentlyAdding;
	private boolean requestflush;
	private Object flushobjekt;
	
	public BufferdInstructionMap() {
		lockadd = false;
		lockaddobjekt = new Object();
		currentlyAdding = false;
		requestflush = false;
		flushobjekt = new Object();
	}
	
	public void add(Object object) {
		requestAddOperation();
		setCurrentlyAdding(true); 
		int hashcode = object.hashCode();
		if(get(hashcode) == null) {
			put(hashcode, object);
		}
		setCurrentlyAdding(false);
		if(requestflush) {
			synchronized (flushobjekt) {
			flushobjekt.notifyAll();	
			}
		}
	}
	
	
	private synchronized void setCurrentlyAdding(boolean currentlyAdding) {
		this.currentlyAdding = currentlyAdding;
	}

	
	public List<Object> flush(){
		lockadd = true;
		if(currentlyAdding) {
			requestflush = true;
			synchronized (flushobjekt) {
				try {
					flushobjekt.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			requestflush = false;
		}
		List<Object> objects = new ArrayList<>();
		objects.addAll(values());
		clear();
		notifyAllAddOperations();
		return objects;
	}
	
	private void requestAddOperation() {
		if(lockadd) {
			synchronized (lockaddobjekt) {
				try {
					lockaddobjekt.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}			
			}
		}	
	}
	
	
	private void notifyAllAddOperations() {
		if(lockadd) {
			lockadd = false;
			synchronized (lockaddobjekt) {
				lockaddobjekt.notifyAll();
			}
		}
	}
	
	
	
}
