package de.starmixcraft.replay.instructions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ReplayInstruction {
	private Instruction instruction;
	private byte[] data;
	
	
	public void writeToSerilizer(ReplayInstructionSerilizer serilizer) {
		serilizer.writeByte(instruction.ordinalbyte());
		serilizer.writeVarInt(data.length);
		for (int i = 0; i < data.length; i++) {
			serilizer.writeByte(data[i]);
		}
	}
	
	
	public void readFromSerilizer(ReplayInstructionSerilizer serilizer) {
		this.instruction = Instruction.fromOrdinalByte(serilizer.readByte());
		int length = serilizer.readVarInt();
		data = new byte[length];
		for (int i = 0; i < length; i++) {
			data[i] = serilizer.readByte();
		}
	}
	
	
}
