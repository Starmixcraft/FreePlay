package de.starmixcraft.replay;

import java.io.File;
import java.util.HashMap;

import de.starmixcraft.replay.backend.Backend;
import de.starmixcraft.replay.recording.Record;
import de.starmixcraft.replay.versions.ServerVersion;
import lombok.Getter;

public class ReplayAdapterManager {
	private HashMap<String, ReplayAdapter> availableAdapters;
	@Getter
	private static ReplayAdapter inUseAdapter;
	@Getter
	private static Record record;
	public ReplayAdapterManager() {
		availableAdapters = new HashMap<>();
	}
	
	 
	public void registerAdapter(ReplayAdapter adapter) {
		availableAdapters.put(adapter.getVersionString(), adapter);
	}
	
	
	public ReplayAdapter setAdapter(String version) {
		ReplayAdapter adapter = availableAdapters.get(version);
		if(adapter == null) {
			return null;
		}
		inUseAdapter = adapter;
		return adapter;
	}
	
	public void StartRecording(Backend backend, ServerVersion serverVersion, File tempfile) {
		record = new Record(backend, serverVersion);
		record.startRecording(tempfile);
	}


	public void initRecordSystem() {
		inUseAdapter.initRecordSystem();
	}


	public void initReplaySystem() {
		inUseAdapter.initReplaySystem();
	}
	
	
	
}
