package de.starmixcraft.replay.utils;

import java.util.Timer;
import java.util.TimerTask;

public class TickIndependentTimer extends TimerTask{

	private Timer timer;
	private Runnable run;
	public TickIndependentTimer(Runnable runnable) {
		timer = new Timer();
		this.run = runnable;
		timer.scheduleAtFixedRate(this, 50, 50);
	}
	@Override
	public void run() {
		run.run();	
	}
	
	
	public void stop() {
		timer.cancel();
	}
}
