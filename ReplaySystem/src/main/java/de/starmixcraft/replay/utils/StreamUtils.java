package de.starmixcraft.replay.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;

public class StreamUtils {
		
		
	public static void writeIntToStream(int i, OutputStream stream) throws IOException {
		  stream.write(Ints.toByteArray(i));
	}
	
	
	public static int readIntFromStream(InputStream inputStream) throws IOException {
		byte[] data = new byte[4];
		inputStream.read(data);
		return Ints.fromByteArray(data);
	}
	
	
	public static void writeLongToStream(long i, OutputStream stream) throws IOException {
		  stream.write(Longs.toByteArray(i));
	}
	
	
	public static long readLongFromStream(InputStream inputStream) throws IOException {
		byte[] data = new byte[8];
		inputStream.read(data);
		return Longs.fromByteArray(data);
		}
}
