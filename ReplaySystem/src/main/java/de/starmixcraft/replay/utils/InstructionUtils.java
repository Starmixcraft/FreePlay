package de.starmixcraft.replay.utils;

import java.util.ArrayList;
import java.util.List;

import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import io.netty.buffer.ByteBufAllocator;

public class InstructionUtils {

	
	public static ArrayList<ReplayInstruction> bytesToList(byte[] data){
		if(data.length == 0) {
			return new ArrayList<>();
		}
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer(ByteBufAllocator.DEFAULT.buffer(data.length));
		instructionSerilizer.writeBytes(data);
		int size = instructionSerilizer.readVarInt();
		ArrayList<ReplayInstruction> instructions = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			ReplayInstruction instruction = new ReplayInstruction();
			instruction.readFromSerilizer(instructionSerilizer);
			instructions.add(instruction);
		}
		return instructions;
	}
	
	public static byte[] listToBytes(List<ReplayInstruction> instructions) {
		if(instructions.isEmpty()) {
			return new byte[0];
		}
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
		instructionSerilizer.writeVarInt(instructions.size());
		for (int i = 0; i < instructions.size(); i++) {
			ReplayInstruction instruction = instructions.get(i);
			instruction.writeToSerilizer(instructionSerilizer);
		}
		return copyFromSerilizer(instructionSerilizer);
	}
	
	
	
	
	
	public static byte[] copyFromSerilizer(ReplayInstructionSerilizer serilizer) {
		byte[] data = new byte[serilizer.writerIndex()];
		if(serilizer.hasArray()) { //Not all bytebufs have a array but system arraycopy is faster because its native so we try to use it
		System.arraycopy(serilizer.array(), 0, data, 0, serilizer.writerIndex());
		}else {
			serilizer.readBytes(data);
		}
		return data;
				
	}
}
