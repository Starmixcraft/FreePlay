package de.starmixcraft.replay.recording.filewriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.starmixcraft.replay.utils.StreamUtils;

public class BasicReplayFileWriter implements ReplayFileWriter{

	private FileOutputStream fileOutputStream;
	private long writtenbytes;
	public BasicReplayFileWriter(File file) {
		try {
			fileOutputStream = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		writtenbytes = 0;
	}
	@Override
	public void close() {
		try {
			fileOutputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void addDataToQuarry(byte[] data) {
		try {
			StreamUtils.writeIntToStream(data.length, fileOutputStream);
			writtenbytes += 4; //The int
			fileOutputStream.write(data); 
			writtenbytes += data.length; //the data
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean isFinished() {
		return true;
	}

	@Override
	public void awaitFinish() {
		
	}
	@Override
	public long getWrittenBytes() {
		return writtenbytes;
	}

}
