package de.starmixcraft.replay.recording.filewriter;

public interface ReplayFileWriter {

	
	
	
	public void close();
	public void addDataToQuarry(byte[] data);
	public boolean isFinished();
	public void awaitFinish();
	public long getWrittenBytes();
}
