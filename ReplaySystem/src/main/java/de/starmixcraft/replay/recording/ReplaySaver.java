package de.starmixcraft.replay.recording;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.common.io.RecursiveDeleteOption;

import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.recording.filewriter.FileCompressor;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replay.replayer.ReplayInfo;
import de.starmixcraft.replay.utils.InstructionUtils;

public class ReplaySaver {

	
	private Record record;
	public ReplaySaver(Record record) {
		this.record = record;
	}
	
	
	
	public void saveSync() {
		record.getBackend().saveReplay(record.getReplayid(), compressedReplay());
	}
	
	
	public void saveAsync() {
		new Thread(()->{
			saveSync();
		});
	}
	
	
	
	private byte[] compressedReplay() {
		record.getFileWriter().awaitFinish(); //wait for finish
		record.getFileWriter().close(); //Close the filewriter
		
		File compressedfile = new File(record.getTempfile().getParentFile(), record.getTempfile().getName() + ".compr"); //create a tem compressd file
		
		ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
		ReplayInfo replayInfo = new ReplayInfo(record.getRecordedticks(), record.getServerVersion()); 
		replayInfo.writeToSerelizer(instructionSerilizer);
		
			FileCompressor compressor = new FileCompressor(record.getTempfile(), compressedfile);
			compressor.compress(InstructionUtils.copyFromSerilizer(instructionSerilizer), record.getFileWriter().getWrittenBytes());

			//		tempfile.delete();
	
			if(compressedfile.length() > Integer.MAX_VALUE - 8) {
			throw new UnsupportedOperationException("The compressed replay is to big to be handled!"); //TODO: find a way to handel replays larger then 1 array, i dont think this is nessesary because 1 array is 2 GB of compressed replay data thats is a lot!
		}
			
		byte[] replaydata = new byte[(int) compressedfile.length()];
		try {
			InputStream inputStream = new FileInputStream(compressedfile);
			inputStream.read(replaydata);
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		compressedfile.delete();
		return replaydata;
	}
}
