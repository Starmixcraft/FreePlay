package de.starmixcraft.replay.recording.filewriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import de.starmixcraft.replay.utils.StreamUtils;
@Deprecated
public class TempFileWriter implements Runnable, ReplayFileWriter{
	private Queue<byte[]> queue;
	private FileOutputStream fileOutputStream;
	public TempFileWriter(File file)  {
		queue = new ConcurrentLinkedQueue<>();
		try {
			fileOutputStream = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		writeToFile();
	}
	@Override
	public void close() {
		try {
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void addDataToQuarry(byte[] data) {
		queue.offer(data);
	}
	private void writeToFile() {
		Thread thread = new Thread(this);
		thread.setName("Replay-File-Writer-Thread");
		thread.setDaemon(true);
		thread.start();
	}
	
	
	
	public synchronized void awaitFinish() {
		if(isFinished()) {
			return;
		}
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean isFinished() {
		return queue.isEmpty();
	}
	
	
	@Override
	public void run() {
		while (true) {
			byte[] data = queue.poll();
			if(data == null) {
				synchronized (this) {this.notifyAll();}
				continue;
			}
			try {
				StreamUtils.writeIntToStream(data.length, fileOutputStream);
				fileOutputStream.write(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public long getWrittenBytes() {
		// TODO Auto-generated method stub
		return -1;
	}

}
