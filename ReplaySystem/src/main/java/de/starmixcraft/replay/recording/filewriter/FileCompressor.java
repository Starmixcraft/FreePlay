package de.starmixcraft.replay.recording.filewriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

import de.starmixcraft.replay.utils.StreamUtils;

public class FileCompressor {

	private FileInputStream inputStream;

	private FileOutputStream outputStream;

	public FileCompressor(File in, File out) {
		try {
			inputStream = new FileInputStream(in);
			outputStream = new FileOutputStream(out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new Error(e);
		}
	}

	public void compress(byte[] frontData, long bytes)  {
		try {
		BZip2CompressorOutputStream compressorOutputStream = new BZip2CompressorOutputStream(outputStream, 3);
		byte[] buf = new byte[1024];
		if (frontData != null) {
			StreamUtils.writeIntToStream(frontData.length, compressorOutputStream);
			compressorOutputStream.write(frontData);
		}
		StreamUtils.writeLongToStream(bytes, compressorOutputStream);
		System.out.println("Bytes in this replay are " + bytes);
		while (inputStream.read(buf) > 0) {
			compressorOutputStream.write(buf);
			compressorOutputStream.flush();
		}
		compressorOutputStream.finish();
		compressorOutputStream.close();
		inputStream.close();
		outputStream.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw new Error(e);
		}
	}
}
