package de.starmixcraft.replay.recording;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CopyOnWriteArrayList;

import de.starmixcraft.replay.ReplayAdapterManager;
import de.starmixcraft.replay.backend.Backend;
import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.recording.filewriter.BasicReplayFileWriter;
import de.starmixcraft.replay.recording.filewriter.FileCompressor;
import de.starmixcraft.replay.recording.filewriter.ReplayFileWriter;
import de.starmixcraft.replay.replayer.ReplayInfo;
import de.starmixcraft.replay.utils.InstructionUtils;
import de.starmixcraft.replay.utils.TickIndependentTimer;
import de.starmixcraft.replay.versions.ServerVersion;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
@Getter(value = AccessLevel.PACKAGE)
public class Record implements Runnable{

	@Getter
	private int recordedticks;
	private CopyOnWriteArrayList<ReplayInstruction> instructions;
	private ReplayFileWriter fileWriter;
	private File tempfile;
	private ReplayInfo replayInfo;
	private ServerVersion serverVersion;
	private Backend backend;
	private String replayid;
	private TickIndependentTimer tickIndependentTimer;
	@Getter
	private boolean recording;
	@Setter
	private Runnable beforeflush;
	public Record(Backend backend, ServerVersion serverVersion) {
		this.serverVersion = serverVersion;
		this.backend = backend;
	}
	public void startRecording(File tempfile) {
		instructions = new CopyOnWriteArrayList<>();
		fileWriter = new BasicReplayFileWriter(tempfile);
		this.tempfile = tempfile;
		this.replayid = backend.getFreeReplayID();
		backend.claimReplayID(replayid);
		tickIndependentTimer = new TickIndependentTimer(this);
		recording = true;
		ReplayAdapterManager.getInUseAdapter().startRecording(this);
		System.out.println("Starting recording");
	}
	
	public void saveRecording() {
		tickIndependentTimer.stop();
		new ReplaySaver(this).saveSync();
	}
	
	public synchronized void flushTick() {
		byte[] data = InstructionUtils.listToBytes(instructions);
		instructions.clear();
		fileWriter.addDataToQuarry(data);
		recordedticks++;
	}
	
	
	public synchronized void addInstruction(Instruction instruction, ReplayInstructionSerilizer serilizer) {
		instructions.add(new ReplayInstruction(instruction, InstructionUtils.copyFromSerilizer(serilizer)));
		
	}
	@Override
	public void run() {
		if(beforeflush != null) {
			beforeflush.run();
		}
		flushTick();		
	}
}
