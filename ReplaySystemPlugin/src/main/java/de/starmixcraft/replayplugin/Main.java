package de.starmixcraft.replayplugin;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.java.JavaPlugin;

import de.starmixcraft.replay.ReplayAdapter;
import de.starmixcraft.replay.ReplayAdapterManager;
import de.starmixcraft.replay.backend.Backend;
import de.starmixcraft.replay.backend.BackendType;
import de.starmixcraft.replay.backend.FileBackend;
import de.starmixcraft.replay.versions.ServerVersion;
import de.starmixcraft.replayplugin.commands.ReplayCommand;
import de.starmixcraft.replaysystem_v1_8_R3.V1_8_R3Adapter;
import io.netty.handler.codec.haproxy.HAProxyProxiedProtocol.AddressFamily;
import lombok.Getter;

public class Main extends JavaPlugin{
	//TODO: Find cause of chunk data los!
	
	@Getter
	private static Main instance;
	@Getter
	private static Backend backend;
	@Getter
	private static ServerVersion serverversion;
	@Getter
	private static PluginMode mode;
	@Override
	public void onEnable() {
		instance = this;
		ReplayAdapterManager adapterManager = new ReplayAdapterManager();
		adapterManager.registerAdapter(new V1_8_R3Adapter(this));
		ReplayAdapter adapter = adapterManager.setAdapter(getVersion(Bukkit.getServer()));
		if(adapter == null) {
			System.out.println("No adapter for this bukkit version!");
			return;
		}
		if(!getDataFolder().exists()) {
			getDataFolder().mkdirs();
		}
		Main.serverversion = adapter.getServerVersion();
		mode = PluginMode.REPLAY;
		BackendType backendType = BackendType.FILE;
		backend = getBackend(backendType);
		if(backend == null) {
			System.out.println("No backend found for " + backendType.toString());
			return;
		}
		if(mode == PluginMode.RECORD) {
			adapterManager.StartRecording(backend, adapter.getServerVersion(), new File(getDataFolder(), "recordtemp.uncompressedrcd"));
			adapter.initRecordSystem();
		}
		
		if(mode == PluginMode.REPLAY) {
			adapter.initReplaySystem();
			getCommand("replay").setExecutor(new ReplayCommand());
		}
		
	}
	
	
	@Override
	public void onDisable() {
		if(mode == PluginMode.RECORD) {
			ReplayAdapterManager.getRecord().saveRecording();
		}
	}
	
	private static Backend getBackend(BackendType backendType) {
		switch (backendType) {
		case FILE:
			return new FileBackend(new File(getInstance().getDataFolder(), "replays"));
		case MONGODB:
			return null; //TODO: implement
		case MYSQL:
			return null; //TODO: implement
		}
		return null;
	}
	
	public static String getVersion(Server server) {
	    final String packageName = server.getClass().getPackage().getName();

	    return packageName.substring(packageName.lastIndexOf('.') + 1);
	}
}
