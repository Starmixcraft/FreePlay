package de.starmixcraft.replayplugin;

public enum PluginMode {
	RECORD,
	REPLAY;
}
