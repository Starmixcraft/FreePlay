package de.starmixcraft.replayplugin.commands;

import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.starmixcraft.replay.ReplayAdapterManager;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replayplugin.Main;

public class ReplayCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player) sender;
		if(args.length == 0) {
			p.sendMessage("/replay list");
			p.sendMessage("/replay watch <replay>");
		}
		if(args.length == 1) {
			if(args[0].equalsIgnoreCase("list")) {
				p.sendMessage("Replays are");
				for(String s : Main.getBackend().getReplays(10)) {
					if(s!=null&&!s.isEmpty())
					p.sendMessage(s);
				}
			}
		}
		if(args.length == 2) {
			Replay replay = new Replay(new Player[] {p}, args[1], Main.getBackend(), Main.getServerversion(), new File(Main.getInstance().getDataFolder(), "playingreplay.temp"));
			p.sendMessage("Loading replay");
			ReplayAdapterManager.getInUseAdapter().initReplay(replay);
			p.sendMessage("Uncompressing replay");
			replay.startReplay();
			p.sendMessage("Starting replay!");
		}
		return false;
	}

}
