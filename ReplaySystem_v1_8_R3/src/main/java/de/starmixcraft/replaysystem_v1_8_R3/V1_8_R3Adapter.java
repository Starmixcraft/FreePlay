package de.starmixcraft.replaysystem_v1_8_R3;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.starmixcraft.replay.ReplayAdapter;
import de.starmixcraft.replay.ReplayAdapterManager;
import de.starmixcraft.replay.recording.Record;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replay.versions.ServerVersion;
import de.starmixcraft.replaysystem_v1_8_R3.nettylistener.PlayerDuplexHandler;
import de.starmixcraft.replaysystem_v1_8_R3.record.BlockListener;
import de.starmixcraft.replaysystem_v1_8_R3.record.ChunkLoadListener;
import de.starmixcraft.replaysystem_v1_8_R3.record.PlayerJoinListener;
import de.starmixcraft.replaysystem_v1_8_R3.replay.ChunkLoadHandler;
import de.starmixcraft.replaysystem_v1_8_R3.replay.EditBlockHandler;
import de.starmixcraft.replaysystem_v1_8_R3.world.V1_8_R3_World;

public class V1_8_R3Adapter extends ReplayAdapter implements Runnable{
	private JavaPlugin plugin;
	
	public V1_8_R3Adapter(JavaPlugin plugin) {
		super("v1_8_R3", ServerVersion.v1_8_R3);
		this.plugin = plugin;
		plugin.getCommand("debug").setExecutor(new DebugCommand());
	}

	@Override
	public void initRecordSystem() {
		Bukkit.getPluginManager().registerEvents(new ChunkLoadListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), plugin);
		PlayerDuplexHandler.eventBus.addhandler(new BlockListener());
	}

	
	public static Record getRecording() {
		return ReplayAdapterManager.getRecord();
	}
	
	public static boolean isRecording() {
		return getRecording().isRecording();
	}

	@Override
	public void initReplay(Replay replay) {
		replay.setWorld(new V1_8_R3_World());	
	}

	@Override
	public void startRecording(Record record) {
		record.setBeforeflush(this);
		if(Bukkit.getWorlds().size() > 0)
		for(Chunk chunk : Bukkit.getWorlds().get(0).getLoadedChunks()) {
			ChunkLoadEvent chunkLoadEvent = new ChunkLoadEvent(chunk, false);
			ChunkLoadListener.getChunkLoadListener().onChunkLoad(chunkLoadEvent);
		}
	}

	@Override
	public void initReplaySystem0() {
		registerInstructionExecutors(new ChunkLoadHandler());
		registerInstructionExecutors(new EditBlockHandler());
	}

	@Override
	public void run() {
		BlockListener.flush(getRecording());
		
	}
	
	
}
