package de.starmixcraft.replaysystem_v1_8_R3.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import net.minecraft.server.v1_8_R3.Entity;

public class UtilReflection {
	public static float getfloat(Object instance, String name) {
		return (Float) get(instance.getClass(), instance, name);
	}

	public static short getshort(Object instance, String name) {
		return (Short) get(instance.getClass(), instance, name);
	}

	public static boolean getboolean(Object instance, String name) {
		return (Boolean) get(instance.getClass(), instance, name);
	}

	public static double getdouble(Object instance, String name) {
		return (Double) get(instance.getClass(), instance, name);
	}

	public static int getint(Object instance, String name) {
		return (Integer) get(instance.getClass(), instance, name);
	}

	public static int getNextEntityID() {
		int id = (int) get(Entity.class, null, "entityCount");
		set(Entity.class, null, "entityCount", id + 1);
		return id;
	}

	public static void set(Object instance, String name, Object value) {
		set(instance.getClass(), instance, name, value);
	}

	private static void set(Class<?> clazz, Object instance, String name, Object value) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			if (Modifier.isFinal(field.getModifiers())) {
				Field modifiersField = Field.class.getDeclaredField("modifiers");
				modifiersField.setAccessible(true);
				modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
			}
			field.set(instance, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object get(Object instance, String name) {
		return get(instance.getClass(), instance, name);
	}

	private static Object get(Class<?> clazz, Object instance, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field.get(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
