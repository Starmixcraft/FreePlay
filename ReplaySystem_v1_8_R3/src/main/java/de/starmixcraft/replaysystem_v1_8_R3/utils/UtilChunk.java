package de.starmixcraft.replaysystem_v1_8_R3.utils;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.common.collect.Lists;

import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replaysystem_v1_8_R3.world.Chunk;
import de.starmixcraft.replaysystem_v1_8_R3.world.ChunkSection;
import net.minecraft.server.v1_8_R3.NibbleArray;
import net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk.ChunkMap;
import net.minecraft.server.v1_8_R3.World;

public class UtilChunk {
	
	public static Chunk load0(World world, ReplayInstructionSerilizer ris) {
		Chunk chunk = new Chunk();
		chunk.setLocX(ris.readVarInt());
		chunk.setLocZ(ris.readVarInt());
		chunk.setHeightMap(ris.readVarIntArray());
		chunk.setTerrainPopulated(ris.readBoolean());
		chunk.setLightCalculated(ris.readBoolean());
		int sections = ris.readVarInt();
		ChunkSection[] chunkSections = new ChunkSection[16];
		for (int i = 0; i < sections; i++) {
			chunkSections[i] = loadChunkSection(world, ris);
		}
		chunk.setSections(chunkSections);
		chunk.setBiomeIndex(ris.readByteArray());
		return chunk;
	}
	
	public static ChunkSection loadChunkSection(World world, ReplayInstructionSerilizer ris) {
		ChunkSection chunkSection = new ChunkSection();
		int ypos = ris.readVarInt();
		chunkSection.setYPos(ypos);
		char[] ids = ris.readCharArray();
		chunkSection.setBlockIds(ids);
		NibbleArray emittedLightArray = new NibbleArray(ris.readByteArray());
		boolean skyLightInData = ris.readBoolean();
		NibbleArray skyLight;
		if(skyLightInData) {
			skyLight = new NibbleArray(ris.readByteArray());
		}else {
			skyLight = new NibbleArray(new byte[emittedLightArray.a().length]);
		}
		chunkSection.setEmittedLight(emittedLightArray);
		if(!world.worldProvider.o())
		chunkSection.setSkyLight(skyLight);
		chunkSection.recalcBlockCounts();
		return chunkSection;
	}
	
	public static void save0(Chunk chunk, World world, ReplayInstructionSerilizer ris) {
		ris.writeVarInt(chunk.getLocX());
		ris.writeVarInt(chunk.getLocZ());
		ris.writeVarIntArray(chunk.getHeightMap());
		ris.writeBoolean(chunk.isTerrainPopulated());
		ris.writeBoolean(chunk.isLightCalculated());
		int sections = 0;
		ArrayList<ChunkSection> arrayList = new ArrayList<>();
		for (ChunkSection chunkSection : chunk.getSections()) {
			if(chunkSection != null)
				arrayList.add(chunkSection);
		}
		sections = arrayList.size();
		ris.writeVarInt(sections);
		for (int i = 0; i < sections; i++) {
			ChunkSection chunkSection = arrayList.get(i);
			saveChunkSection(chunkSection, world, ris);
		}
		ris.writeByteArray(chunk.getBiomeIndex());
	}
	
	
	public static void saveChunkSection(ChunkSection chunkSection, World world, ReplayInstructionSerilizer ris) {
		ris.writeVarInt(chunkSection.getYPosition());
		ris.writeCharArray(chunkSection.getIdArray());
		ris.writeByteArray(chunkSection.getEmittedLightArray().a());
		boolean flag = !world.worldProvider.o();
		if (flag && chunkSection.getSkyLightArray() != null) {
			ris.writeBoolean(true);
			ris.writeByteArray(chunkSection.getSkyLightArray().a());
        } else {
        	ris.writeBoolean(false);
        }
		
	}
	    
	    
	   protected static int a(int i, boolean flag, boolean flag1) {
	        int j = i * 2 * 16 * 16 * 16;
	        int k = i * 16 * 16 * 16 / 2;
	        int l = flag ? i * 16 * 16 * 16 / 2 : 0;
	        int i1 = flag1 ? 256 : 0;

	        return j + k + l + i1;
	    }


	    public static ChunkMap getChunkMap(Chunk chunk) {
	    	boolean flag = true;
	    	boolean flag1 = true;
	    	int i = 65535;
	    	return a(chunk, flag, flag1, i);
	    }
	    public static ChunkMap a(Chunk chunk, boolean flag, boolean flag1, int i) {
	        ChunkSection[] achunksection = chunk.getSections();
	        ChunkMap packetplayoutmapchunk_chunkmap = new ChunkMap();
	        ArrayList<ChunkSection> arraylist = Lists.newArrayList();

	        int j;

	        for (j = 0; j < achunksection.length; ++j) {
	            ChunkSection chunksection = achunksection[j];

	            if (chunksection != null && (!flag || !chunksection.a()) && (i & 1 << j) != 0) {
	                packetplayoutmapchunk_chunkmap.b |= 1 << j;
	                arraylist.add(chunksection);
	            }
	        }

	        packetplayoutmapchunk_chunkmap.a = new byte[a(Integer.bitCount(packetplayoutmapchunk_chunkmap.b), flag1, flag)];
	        j = 0;
	        Iterator<ChunkSection> iterator = arraylist.iterator();

	        ChunkSection chunksection1;

	        while (iterator.hasNext()) {
	            chunksection1 = (ChunkSection) iterator.next();
	            char[] achar = chunksection1.getIdArray();
	            char[] achar1 = achar;
	            int k = achar.length;

	            for (int l = 0; l < k; ++l) {
	                char c0 = achar1[l];

	                packetplayoutmapchunk_chunkmap.a[j++] = (byte) (c0 & 255);
	                packetplayoutmapchunk_chunkmap.a[j++] = (byte) (c0 >> 8 & 255);
	            }
	        }

	        for (iterator = arraylist.iterator(); iterator.hasNext(); j = a(chunksection1.getEmittedLightArray().a(), packetplayoutmapchunk_chunkmap.a, j)) {
	            chunksection1 = (ChunkSection) iterator.next();
	        }

	        if (flag1) {
	            for (iterator = arraylist.iterator(); iterator.hasNext(); j = a(chunksection1.getSkyLightArray().a(), packetplayoutmapchunk_chunkmap.a, j)) {
	                chunksection1 = (ChunkSection) iterator.next();
	            }
	        }

	        if (flag) {
	            a(chunk.getBiomeIndex(), packetplayoutmapchunk_chunkmap.a, j);
	        }

	        return packetplayoutmapchunk_chunkmap;
	    }

	    private static int a(byte[] abyte, byte[] abyte1, int i) {
	        System.arraycopy(abyte, 0, abyte1, i, abyte.length);
	        return i + abyte.length;
	    } 
	    }
