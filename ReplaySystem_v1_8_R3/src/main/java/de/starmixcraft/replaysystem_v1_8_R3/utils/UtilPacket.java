package de.starmixcraft.replaysystem_v1_8_R3.utils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityVelocity;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntity;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;

public class UtilPacket {
	
	public static PacketPlayOutEntityVelocity createDefaulVelocityPacket(int id, Vector vector) {
		PacketPlayOutEntityVelocity entityVelocity = new PacketPlayOutEntityVelocity(id,vector.getX(),vector.getY(),vector.getZ());
		return entityVelocity;
	}
	
	public static PacketPlayOutEntityDestroy createDefaultDestroyPacket(int id) {
		return new PacketPlayOutEntityDestroy(new int[] {id});
	}
	
	
	public static PacketPlayOutEntityMetadata createDefaultEntityMetadataPacket(int id, DataWatcher dataWatcher) {
		PacketPlayOutEntityMetadata entityMetadata = new PacketPlayOutEntityMetadata();
		UtilReflection.set(entityMetadata, "a", id);
		UtilReflection.set(entityMetadata, "b", dataWatcher.c());
		return entityMetadata;
	}
	
	
	public static PacketPlayOutSpawnEntity createDefaultEntitySpawnPacket(int id, Location loc, int type) {
		PacketPlayOutSpawnEntity spawnEntity = new PacketPlayOutSpawnEntity();
		UtilReflection.set(spawnEntity, "a", id);
		UtilReflection.set(spawnEntity, "b", toFixedPoint(loc.getX()));
		UtilReflection.set(spawnEntity, "c", toFixedPoint(loc.getY()));
		UtilReflection.set(spawnEntity, "d", toFixedPoint(loc.getZ()));
		UtilReflection.set(spawnEntity, "h", toAngle(loc.getPitch()));
		UtilReflection.set(spawnEntity, "i", toAngle(loc.getYaw()));
		UtilReflection.set(spawnEntity, "k", 0);
		UtilReflection.set(spawnEntity, "j", type);
		return spawnEntity;
	}
	public static PacketPlayOutEntityTeleport createDefaultTeleportPacket(int id, Location loc) {
	PacketPlayOutEntityTeleport entityTeleport = new PacketPlayOutEntityTeleport();
	UtilReflection.set(entityTeleport, "a", id);
	UtilReflection.set(entityTeleport, "b", toFixedPoint(loc.getX()));
	UtilReflection.set(entityTeleport, "c", toFixedPoint(loc.getY()));
	UtilReflection.set(entityTeleport, "d", toFixedPoint(loc.getZ()));
	UtilReflection.set(entityTeleport, "e", toAngle(loc.getYaw()));
	UtilReflection.set(entityTeleport, "f", toAngle(loc.getPitch()));
	UtilReflection.set(entityTeleport, "g", true);
	return entityTeleport;
	}
	
	
	public static PacketPlayOutSpawnEntityLiving createDefaultMobSpawnPacket(int id, Location loc, int entitytype, DataWatcher dataWatcher) {
		PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving();
		UtilReflection.set(packet, "a", id);
		UtilReflection.set(packet, "b", (byte) entitytype);
		UtilReflection.set(packet, "c", toFixedPoint(loc.getX()));
		UtilReflection.set(packet, "d", toFixedPoint(loc.getY()));
		UtilReflection.set(packet, "e", toFixedPoint(loc.getZ()));
		UtilReflection.set(packet, "f", 0);
		UtilReflection.set(packet, "g", 0);
		UtilReflection.set(packet, "h", 0);
		UtilReflection.set(packet, "i", (byte)toAngle(loc.getYaw()));
		UtilReflection.set(packet, "j", (byte)toAngle(loc.getPitch()));
		UtilReflection.set(packet, "k", (byte)toAngle(loc.getYaw()));
		UtilReflection.set(packet, "l", dataWatcher);
		return packet;
	}
	
	
	private static int toFixedPoint(double d) {
		return MathHelper.floor(d * 32.0);
	}
	
	private static int toAngle(float f) {
		return MathHelper.d(f * 256.0f / 360.0f);
	}
}
