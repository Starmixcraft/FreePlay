package de.starmixcraft.replaysystem_v1_8_R3.utils;


import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replaysystem_v1_8_R3.world.Block;

public class UtilBlock {

	
	
	
	public static void writeBlock(Block block, ReplayInstructionSerilizer instructionSerilizer) {
		instructionSerilizer.writeVarInt(block.getChunkx());
		instructionSerilizer.writeVarInt(block.getChunkz());
		instructionSerilizer.writeVarInt(block.getX());
		instructionSerilizer.writeVarInt(block.getY());
		instructionSerilizer.writeVarInt(block.getZ());
		instructionSerilizer.writeVarInt(block.getMatid());
		instructionSerilizer.writeByte(block.getData());
	}
	
	
	public static Block readBlock(ReplayInstructionSerilizer instructionSerilizer) {
		Block block = new Block();
		block.setChunkx(instructionSerilizer.readVarInt());
		block.setChunkz(instructionSerilizer.readVarInt());
		block.setX(instructionSerilizer.readVarInt());
		block.setY(instructionSerilizer.readVarInt());
		block.setZ(instructionSerilizer.readVarInt());
		block.setMatid(instructionSerilizer.readVarInt());
		block.setData(instructionSerilizer.readByte());
		return block;
	}
	
	
	
	public static Block fromBukkitBlock(org.bukkit.block.Block bukkitblock) {
		Block retblock = new Block();
		retblock.setData(bukkitblock.getData());
		retblock.setMatid(bukkitblock.getTypeId());
		retblock.setChunkx(bukkitblock.getChunk().getX());
		retblock.setChunkz(bukkitblock.getChunk().getZ());
		retblock.setX(bukkitblock.getX());
		retblock.setY(bukkitblock.getY());
		retblock.setZ(bukkitblock.getZ());
		return retblock;
	}
}
