package de.starmixcraft.replaysystem_v1_8_R3;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replay.replayer.ReplayManager;
import de.starmixcraft.replaysystem_v1_8_R3.world.V1_8_R3_World;

public class DebugCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (args.length == 0) {
			sender.sendMessage("/debug chunk load");
			sender.sendMessage("/debug chunk unload");
			sender.sendMessage("/debug chunk isloaded");
			sender.sendMessage("/debug chunk isavailable");
		}

		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("chunk")) {
				if (args[1].equalsIgnoreCase("load")) {
					Replay replay = ReplayManager.getInstance().getReplay(p);
					((V1_8_R3_World)replay.getWorld()).getChunk(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ()).load(p);
				}
				if (args[1].equalsIgnoreCase("unload")) {
					Replay replay = ReplayManager.getInstance().getReplay(p);
					((V1_8_R3_World)replay.getWorld()).getChunk(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ()).unload(p);
				}
				if (args[1].equalsIgnoreCase("isloaded")) {
					Replay replay = ReplayManager.getInstance().getReplay(p);
					p.sendMessage(((V1_8_R3_World)replay.getWorld()).getChunk(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ()).isSentToClient(p) ? "true" : "false");
				}
				if (args[1].equalsIgnoreCase("isavailable")) {
					Replay replay = ReplayManager.getInstance().getReplay(p);
					p.sendMessage(((V1_8_R3_World)replay.getWorld()).getChunk(p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ()) != null ? "true" : "false");
				}
			}
		}
		return false;
	}

}
