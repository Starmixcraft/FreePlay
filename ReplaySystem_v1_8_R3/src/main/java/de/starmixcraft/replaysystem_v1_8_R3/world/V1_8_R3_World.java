package de.starmixcraft.replaysystem_v1_8_R3.world;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import de.starmixcraft.replay.world.World;
import lombok.Getter;

@Getter
public class V1_8_R3_World implements World {
	private ArrayList<Chunk> chunks;

	public V1_8_R3_World() {
		chunks = new ArrayList<>();

	}

	public Chunk getChunk(int x, int z) {
		for (Chunk chunk : chunks) {
			if (chunk.getLocX() == x && chunk.getLocZ() == z) {
				return chunk;
			}
		}
		return null;
	}

	public void onLoad(Chunk chunk) {
		if (getChunk(chunk.getLocX(), chunk.getLocZ()) == null) {
			chunks.add(chunk);
		}
	}

	public void resendAll() {
		for (Chunk chunk : chunks) {
			chunk.getLoaded().clear();
		}
	}

	public void tick(Player[] players) {
		for (Chunk chunk : chunks) {
			List<Player> unload = Lists.newArrayList();
			List<Player> load = Lists.newArrayList();
			for (Player p : players) {
				boolean ics = getIntendedChunkState(p, chunk);
				if (ics != chunk.isSentToClient(p)) {
					if (ics) {
						load.add(p);
					} else {
						unload.add(p);
					}
				}
			}
			chunk.load(load.toArray(new Player[0])); //Made load and unload more efficent!
			chunk.unload(unload.toArray(new Player[0]));
			chunk.updateChunk();
		}
	}

	private boolean getIntendedChunkState(Player p, Chunk chunk) {
		int chunkPlayerX = p.getLocation().getChunk().getX();
		int chunkPlayerZ = p.getLocation().getChunk().getZ();

		int chunkX = chunk.getLocX();
		int chunkZ = chunk.getLocZ();

		int diffX = chunkX < chunkPlayerX ? chunkPlayerX - chunkX : chunkX - chunkPlayerX;
		int diffZ = chunkZ < chunkPlayerZ ? chunkPlayerZ - chunkZ : chunkZ - chunkPlayerZ;
		if (diffX < 9 && diffZ < 9) {
			return true;
		} else {
			return false;
		}
	}

}
