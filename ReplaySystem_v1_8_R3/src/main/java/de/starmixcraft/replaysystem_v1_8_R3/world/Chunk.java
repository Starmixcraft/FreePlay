package de.starmixcraft.replaysystem_v1_8_R3.world;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilChunk;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilReflection;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.ChunkCoordIntPair;
import net.minecraft.server.v1_8_R3.IBlockData;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockChange;
import net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk;
import net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk.ChunkMap;
import net.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange;
import net.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange.MultiBlockChangeInfo;

@Getter
@Setter
public class Chunk {
	private ChunkSection[] sections = new ChunkSection[16];
    private byte[] biomeIndex = new byte[256];
    private int[] heightMap;
    private int locX;
    private int locZ;
    private boolean terrainPopulated;
    private boolean lightCalculated;
    private short[] dirty = new short[64];
    private int dirtycount;
    private ConcurrentHashMap<Player, Object> loaded;
    private static final Object dummy = new Object();
    
    public Chunk() {
    	loaded = new ConcurrentHashMap<>();
    }
    
    public Chunk(net.minecraft.server.v1_8_R3.Chunk chunk) {
    	if(!chunk.u()) {
    	chunk.initLighting();
    	}
    	this.locX = chunk.locX;
    	this.locZ = chunk.locZ;
    	this.biomeIndex = chunk.getBiomeIndex();
    	this.heightMap = chunk.heightMap;
    	this.terrainPopulated = chunk.isDone();
    	this.lightCalculated = chunk.u();
    	for(int i = 0; i<16;i++) {
    		net.minecraft.server.v1_8_R3.ChunkSection chunkSection = chunk.getSections()[i];
    		if(chunkSection != null) {
    			sections[i] = new ChunkSection(chunkSection);
    		}
    	}
    	loaded = new ConcurrentHashMap<>();
    }
    
    
    public void brodcastToLoaded(Packet<?> packet) {
    	loaded.forEach((p, ad /*Ignore*/) ->{((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);});
    }
    
    public Chunk(int i, int j) {
        this.locX = i;
        this.locZ = j;
        this.heightMap = new int[256];
        Arrays.fill(this.biomeIndex, (byte)-1);
        loaded = new ConcurrentHashMap<>();
    }
    
    
    public void setBlock(Block block) {
    	setBlockWithoutDirty(block);
    	makeDirty(block);
    }
    
    public void makeDirty(Block block) {
    	if(dirtycount < 64) {
    		short blocklock = (short) ((block.getX() & 15) << 12 | (block.getZ() & 15) << 8 | block.getY());
    		for (int i = 0; i < dirty.length; i++) {
				if(dirty[0] == blocklock) {
					return;
				}
			}
    		this.dirty[dirtycount++] = blocklock;
    	}
    }
    
    public void setBlockWithoutDirty(Block block) {
    	int chunkx = block.getX() & 15;
    	int chunky = block.getY() & 15;
    	int chunkz = block.getZ() & 15;
    	int chunksectionid = block.getY() >> 4;
    	ChunkSection chunkSection = sections[chunksectionid];
    	if(chunkSection == null) {
    		ChunkSection chunksection = new ChunkSection(chunksectionid, true);
    		sections[chunksectionid] = chunksection;
    		chunkSection = chunksection;
    	}
    	IBlockData blockData = net.minecraft.server.v1_8_R3.Block.d.a(block.getMatid() << 4 | (block.getData() & 15));
    	chunkSection.setType(chunkx, chunky, chunkz, blockData);
    }
    
    public void unload(Player... players) {
    	PacketPlayOutMapChunk chunk = new PacketPlayOutMapChunk();
    	ChunkMap chunkMap = new ChunkMap();
    	chunkMap.b = 0;
    	chunkMap.a = new byte[0];
		UtilReflection.set(chunk, "a", this.getLocX());
		UtilReflection.set(chunk, "b", this.getLocZ());
		UtilReflection.set(chunk, "c", chunkMap);
		UtilReflection.set(chunk, "d", true);
		for (Player p : players) {
			if(loaded.remove(p) == null) {
				System.out.println("Warning a chunk was unloaded that was not loaded in the player!");
			}
			((CraftPlayer)p).getHandle().playerConnection.sendPacket(chunk);
		}
    }
    
	public void load(Player... players) {
		Packet<?> chunk = createChunkPacket();
		for (Player p : players) {
		loaded.put(p, dummy);
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(chunk);
		}
	}
	
	
	private Packet<?> createChunkPacket(){
		PacketPlayOutMapChunk chunk = new PacketPlayOutMapChunk();
		ChunkMap chunkMap = UtilChunk.getChunkMap(this);
		UtilReflection.set(chunk, "a", this.getLocX());
		UtilReflection.set(chunk, "b", this.getLocZ());
		UtilReflection.set(chunk, "c", chunkMap);
		UtilReflection.set(chunk, "d", true);
		return chunk;
	}
	
	public boolean isSentToClient(Player p) {
		return loaded.containsKey(p);
	}
	
	
	public void updateChunk() {
		if(dirtycount == 0) {
			return;
		}else
		if(dirtycount == 1) {
			brodcastBlockChange();
		}else
		if(dirtycount < 64) {
			brodcastMuliBlockChange();
		}else {
			brodcastChunkUpdate();
		}
		dirtycount = 0;
	}
	
	
	public void brodcastBlockChange() {
		int x = (this.dirty[0] >> 12 & 15);
		int y = this.dirty[0] & 255;
		int z = (this.dirty[0] >> 8 & 15);
		BlockPosition blockposition = new BlockPosition(x + this.locX*16 , y, z + this.locZ * 16);
		int chunksectiony = y >> 4;
    	int yinsec = y & 15;
    	ChunkSection chunkSection = sections[chunksectiony];
    	if(chunkSection == null) {
    		System.out.println("Cant brodcast block change packet no chunk section found!");
    		return;
    	}
    	IBlockData blockData = chunkSection.getType(x, yinsec, z);
    	PacketPlayOutBlockChange blockChange = new PacketPlayOutBlockChange();
    	UtilReflection.set(blockChange, "a", blockposition);
    	UtilReflection.set(blockChange, "block", blockData);
    	brodcastToLoaded(blockChange);
	}
	
	public void brodcastMuliBlockChange() {
		PacketPlayOutMultiBlockChange blockChange = new PacketPlayOutMultiBlockChange();
		MultiBlockChangeInfo[] info = new PacketPlayOutMultiBlockChange.MultiBlockChangeInfo[dirtycount];
		UtilReflection.set(blockChange, "a", new ChunkCoordIntPair(locX, locZ));
		for (int i = 0; i < dirtycount; i++) {
			int x = (this.dirty[i] >> 12 & 15);
			int y = this.dirty[i] & 255;
			int z = (this.dirty[i] >> 8 & 15);
			int chunksectiony = y >> 4;
	    	int yinsec = y & 15;
	    	ChunkSection chunkSection = sections[chunksectiony];
	    	if(chunkSection == null) {
	    		System.out.println("Cant brodcast block change packet no chunk section found!");
	    		return;
	    	}
			info[i] = blockChange.new MultiBlockChangeInfo(dirty[i], chunkSection.getType(x, yinsec, z));
		}
		UtilReflection.set(blockChange, "b", info);
		brodcastToLoaded(blockChange);
	}
	
	public void brodcastChunkUpdate() {
		brodcastToLoaded(createChunkPacket());
	}
}

