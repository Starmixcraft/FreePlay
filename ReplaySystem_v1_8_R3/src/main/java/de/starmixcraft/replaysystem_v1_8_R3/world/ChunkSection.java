package de.starmixcraft.replaysystem_v1_8_R3.world;

import java.util.ArrayList;

import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.Block;
import net.minecraft.server.v1_8_R3.Blocks;
import net.minecraft.server.v1_8_R3.IBlockData;
import net.minecraft.server.v1_8_R3.NibbleArray;
@NoArgsConstructor
public class ChunkSection {
	@Setter
    private int yPos;
    private int nonEmptyBlockCount;
    private int tickingBlockCount;
    private char[] blockIds;
    private NibbleArray emittedLight;
    private NibbleArray skyLight;

    public ChunkSection(int i, boolean flag) {
        this.yPos = i;
        this.blockIds = new char[4096];
        this.emittedLight = new NibbleArray();
        if (flag) {
            this.skyLight = new NibbleArray();
        }
    }
    
    
    public ChunkSection(net.minecraft.server.v1_8_R3.ChunkSection chunkSection) {
    	this.yPos = chunkSection.getYPosition();
    	this.blockIds = chunkSection.getIdArray();
    	this.emittedLight = chunkSection.getEmittedLightArray();
    	this.skyLight = chunkSection.getSkyLightArray();
    	recalcBlockCounts();
    }

    public ChunkSection(int y, boolean flag, char[] blockIds) {
        this.yPos = y;
        this.blockIds = blockIds;
        this.emittedLight = new NibbleArray();
        if (flag) {
            this.skyLight = new NibbleArray();
        }
        this.recalcBlockCounts();
    }

    public IBlockData getType(int x, int y, int z) {
        IBlockData iblockdata = Block.d.a(this.blockIds[y << 8 | z << 4 | x]);
        return iblockdata != null ? iblockdata : Blocks.AIR.getBlockData();
    }

    public void setType(int x, int y, int z, IBlockData iblockdata) {
        IBlockData iblockdata1 = this.getType(x, y, z);
        Block block = iblockdata1.getBlock();
        Block block1 = iblockdata.getBlock();
        if (block != Blocks.AIR) {
            --this.nonEmptyBlockCount;
            if (block.isTicking()) {
                --this.tickingBlockCount;
            }
        }
        if (block1 != Blocks.AIR) {
            ++this.nonEmptyBlockCount;
            if (block1.isTicking()) {
                ++this.tickingBlockCount;
            }
        }
        this.blockIds[y << 8 | z << 4 | x] = (char)Block.d.b(iblockdata);
    }
    
    
    public Block b(int i, int j, int k) {
        return this.getType(i, j, k).getBlock();
    }

    public int c(int i, int j, int k) {
        IBlockData iblockdata = this.getType(i, j, k);
        return iblockdata.getBlock().toLegacyData(iblockdata);
    }

    public boolean a() {
        if (this.nonEmptyBlockCount == 0) {
            return true;
        }
        return false;
    }

    public boolean shouldTick() {
        if (this.tickingBlockCount > 0) {
            return true;
        }
        return false;
    }

    public int getYPosition() {
        return this.yPos;
    }

    public void a(int i, int j, int k, int l) {
        this.skyLight.a(i, j, k, l);
    }

    public int d(int i, int j, int k) {
        return this.skyLight.a(i, j, k);
    }

    public void b(int i, int j, int k, int l) {
        this.emittedLight.a(i, j, k, l);
    }

    public int e(int i, int j, int k) {
        return this.emittedLight.a(i, j, k);
    }

    public void recalcBlockCounts() {
        this.nonEmptyBlockCount = 0;
        this.tickingBlockCount = 0;
        int i = 0;
        while (i < 16) {
            int j = 0;
            while (j < 16) {
                int k = 0;
                while (k < 16) {
                    Block block = this.b(i, j, k);
                    if (block != Blocks.AIR) {
                        ++this.nonEmptyBlockCount;
                        if (block.isTicking()) {
                            ++this.tickingBlockCount;
                        }
                    }
                    ++k;
                }
                ++j;
            }
            ++i;
        }
    }

    public char[] getIdArray() {
        return this.blockIds;
    }

    public void setBlockIds(char[] achar) {
        this.blockIds = achar;
    }

    public NibbleArray getEmittedLightArray() {
        return this.emittedLight;
    }

    public NibbleArray getSkyLightArray() {
        return this.skyLight;
    }

    public void setEmittedLight(NibbleArray nibblearray) {
        this.emittedLight = nibblearray;
    }

    public void setSkyLight(NibbleArray nibblearray) {
        this.skyLight = nibblearray;
    }
}

