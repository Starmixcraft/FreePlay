package de.starmixcraft.replaysystem_v1_8_R3.world;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Block {
	private int chunkx,chunkz;
	private int matid;
	private byte data;
	private int x, y, z;
}
