package de.starmixcraft.replaysystem_v1_8_R3.replay;

import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.replayer.InstructionExecutor;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilBlock;
import de.starmixcraft.replaysystem_v1_8_R3.world.Block;
import de.starmixcraft.replaysystem_v1_8_R3.world.Chunk;
import de.starmixcraft.replaysystem_v1_8_R3.world.V1_8_R3_World;

public class EditBlockHandler {

	
	@InstructionExecutor(instruction = Instruction.EDITBLOCK)
	public void handelEditBlock(Replay replay, ReplayInstructionSerilizer instructionSerilizer) {
	Block block = UtilBlock.readBlock(instructionSerilizer);
	V1_8_R3_World world = (V1_8_R3_World) replay.getWorld();
	Chunk chunk = world.getChunk(block.getChunkx(), block.getChunkz());
	chunk.setBlock(block);
	}
	
	@InstructionExecutor(instruction = Instruction.EDITBLOCKS)
	public void handelEditBlocks(Replay replay, ReplayInstructionSerilizer instructionSerilizer) {
		int blocktofetch = instructionSerilizer.readVarInt();
		for (int i = 0; i < blocktofetch; i++) {
			Block block = UtilBlock.readBlock(instructionSerilizer);
			V1_8_R3_World world = (V1_8_R3_World) replay.getWorld();
			Chunk chunk = world.getChunk(block.getChunkx(), block.getChunkz());
			chunk.setBlock(block);
		}
		
	}
	
	
	
	
	
}
