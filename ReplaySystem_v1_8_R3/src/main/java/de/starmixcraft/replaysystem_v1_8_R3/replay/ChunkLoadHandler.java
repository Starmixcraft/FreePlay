package de.starmixcraft.replaysystem_v1_8_R3.replay;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.replayer.InstructionExecutor;
import de.starmixcraft.replay.replayer.Replay;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilChunk;
import de.starmixcraft.replaysystem_v1_8_R3.world.Chunk;
import de.starmixcraft.replaysystem_v1_8_R3.world.V1_8_R3_World;

public class ChunkLoadHandler {
	@InstructionExecutor(instruction = Instruction.LOADCHUNK)
	public void loadChunk(Replay replay, ReplayInstructionSerilizer serilizer) {
		Chunk chunk = UtilChunk.load0(((CraftWorld)Bukkit.getWorlds().get(0)).getHandle().b(), serilizer);
		((V1_8_R3_World)replay.getWorld()).onLoad(chunk);
	}
}
