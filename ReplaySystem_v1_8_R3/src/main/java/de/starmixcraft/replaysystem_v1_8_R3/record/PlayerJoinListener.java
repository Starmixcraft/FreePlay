package de.starmixcraft.replaysystem_v1_8_R3.record;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.starmixcraft.replaysystem_v1_8_R3.nettylistener.PlayerDuplexHandler;

public class PlayerJoinListener implements Listener {

	@EventHandler
	public static void onJoin(PlayerJoinEvent e) {
		if(!e.getPlayer().spigot().getCollidesWithEntities()) {
			return;//Specktator
		}
		new PlayerDuplexHandler(e.getPlayer());
	}
}
