package de.starmixcraft.replaysystem_v1_8_R3.record;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replay.recording.Record;
import de.starmixcraft.replaysystem_v1_8_R3.V1_8_R3Adapter;
import de.starmixcraft.replaysystem_v1_8_R3.nettylistener.PacketListener;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilBlock;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilReflection;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockChange;
import net.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange;
import net.minecraft.server.v1_8_R3.PacketPlayOutMultiBlockChange.MultiBlockChangeInfo;

public class BlockListener {
	private static HashMap<Integer, PacketPlayOutBlockChange> datablockchange = new HashMap<>();
	private static HashMap<Integer, PacketPlayOutMultiBlockChange> datamultiblockchange = new HashMap<>();
	//private static HashMap<Integer, PacketPlayOutMapChunk> datachunkpacket = new HashMap<>(); cant decifer this packet
	
	
	public static void flush(Record record) {
		if(!(Bukkit.getWorlds().size() > 0)) {
			return;
		}
		World world = Bukkit.getWorlds().get(0);
		if(!datablockchange.isEmpty())
		for(PacketPlayOutBlockChange blockChange : datablockchange.values()) {
			BlockPosition blockPosition = (BlockPosition) UtilReflection.get(blockChange, "a");
			changeBlock(world.getBlockAt(blockPosition.getX(), blockPosition.getY(), blockPosition.getZ()));
		}
		datablockchange.clear();
		if(!datamultiblockchange.isEmpty())
		for(PacketPlayOutMultiBlockChange blockChange : datamultiblockchange.values()) {
			MultiBlockChangeInfo[] data = (MultiBlockChangeInfo[]) UtilReflection.get(blockChange, "b");
			Block[] blocks = new Block[data.length];
			int i = 0;
			for(MultiBlockChangeInfo a : data) {
				BlockPosition blockPosition = a.a();
				blocks[i] = world.getBlockAt(blockPosition.getX(), blockPosition.getY(), blockPosition.getZ());
				i++;
			}
		changeBlock(blocks);	
		}
		datamultiblockchange.clear();
		
		
	}
	
	@PacketListener(packet = PacketPlayOutBlockChange.class)
	public static void handelBlockChange(Player p, PacketPlayOutBlockChange b) {
		if(p.getWorld() == Bukkit.getWorlds().get(0)) {
			datablockchange.put(b.hashCode(), b);
		}
	}
	
	@PacketListener(packet = PacketPlayOutMultiBlockChange.class)
	public static void handelMultiBlockChange(Player p, PacketPlayOutMultiBlockChange b) {
		if(p.getWorld() == Bukkit.getWorlds().get(0)) {
			datamultiblockchange.put(b.hashCode(), b);
		}
	}
	
	public static void changeBlock(Block... b) {
		if(!V1_8_R3Adapter.isRecording()) {
			return;
		}
		
		if(b.length == 1) {
			ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
			UtilBlock.writeBlock(UtilBlock.fromBukkitBlock(b[0]), instructionSerilizer);
			V1_8_R3Adapter.getRecording().addInstruction(Instruction.EDITBLOCK, instructionSerilizer);
		}else {
			ReplayInstructionSerilizer instructionSerilizer = new ReplayInstructionSerilizer();
			instructionSerilizer.writeVarInt(b.length);
			for(Block block : b)
			UtilBlock.writeBlock(UtilBlock.fromBukkitBlock(block), instructionSerilizer);
			V1_8_R3Adapter.getRecording().addInstruction(Instruction.EDITBLOCKS, instructionSerilizer);
		}
		
		
	}
	

	
	
}
