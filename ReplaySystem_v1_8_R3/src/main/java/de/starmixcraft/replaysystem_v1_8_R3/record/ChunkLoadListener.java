package de.starmixcraft.replaysystem_v1_8_R3.record;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.EventHandler;

import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

import de.starmixcraft.replay.instructions.Instruction;
import de.starmixcraft.replay.instructions.ReplayInstructionSerilizer;
import de.starmixcraft.replaysystem_v1_8_R3.V1_8_R3Adapter;
import de.starmixcraft.replaysystem_v1_8_R3.utils.UtilChunk;
import de.starmixcraft.replaysystem_v1_8_R3.world.Chunk;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

public class ChunkLoadListener implements Listener{
	private ArrayList<ChunkCodePair> allreadyloaded;
	@Getter
	private static ChunkLoadListener chunkLoadListener;
	
	public ChunkLoadListener() {
		allreadyloaded = new ArrayList<>();
		chunkLoadListener = this;
	}
	
	public void startRecording() {
		
	}
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e) {
		if(!V1_8_R3Adapter.isRecording()) {
			return;
		}
		if(Bukkit.getWorlds().get(0) != e.getWorld()) {
			return;
		}
		ChunkCodePair chunkCodePair = new ChunkCodePair(e.getChunk().getX(), e.getChunk().getZ());
		if(!allreadyloaded.contains(chunkCodePair)) {
			allreadyloaded.add(chunkCodePair);
			ReplayInstructionSerilizer serilizer = new ReplayInstructionSerilizer();
			net.minecraft.server.v1_8_R3.Chunk chunk = ((CraftChunk)e.getChunk()).getHandle();
			UtilChunk.save0(new Chunk(chunk), chunk.getWorld(), serilizer);
			V1_8_R3Adapter.getRecording().addInstruction(Instruction.LOADCHUNK, serilizer);
		}
	}
	
	
	
	
	
	@AllArgsConstructor
	private static class ChunkCodePair{
		private int x;
		private int z;
		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof ChunkCodePair)) {
				return false;
			}
			ChunkCodePair codePair = (ChunkCodePair) obj;
			return x == codePair.x && z == codePair.z;
		}
	}
}
