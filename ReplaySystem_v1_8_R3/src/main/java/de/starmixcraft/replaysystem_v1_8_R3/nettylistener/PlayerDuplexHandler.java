package de.starmixcraft.replaysystem_v1_8_R3.nettylistener;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_8_R3.Packet;

public class PlayerDuplexHandler extends ChannelDuplexHandler{

	private Player p;
	public static PacketEventBus eventBus;
	public PlayerDuplexHandler(Player p) {
		this.p = p;
		((CraftPlayer)p).getHandle().playerConnection.networkManager.channel.pipeline().addAfter("packet_handler", "freeplay_handler", this);
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		eventBus.handel(p, (Packet<?>) msg);
		super.channelRead(ctx, msg);
	}
	
	
	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		eventBus.handel(p, (Packet<?>) msg);
		super.write(ctx, msg, promise);
	}
	
	static {
		eventBus = new PacketEventBus();
	}
}
