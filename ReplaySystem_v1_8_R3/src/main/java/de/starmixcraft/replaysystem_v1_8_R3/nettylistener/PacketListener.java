package de.starmixcraft.replaysystem_v1_8_R3.nettylistener;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.minecraft.server.v1_8_R3.Packet;


@Retention(RUNTIME)
@Target(METHOD)
public @interface PacketListener {
	public Class<? extends Packet<?>> packet();
}
