package de.starmixcraft.replaysystem_v1_8_R3.nettylistener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import net.minecraft.server.v1_8_R3.Packet;

public class PacketEventBus {
	private HashMap<Class<? extends Packet<?>>, HandlerMethod> handlers;
	
	
	public PacketEventBus() {
		handlers = new HashMap<>();
	}
	
	
	public void handel(Player p, Packet<?> packet) {
		HandlerMethod handlerMethod = handlers.get(packet.getClass());
		if(handlerMethod == null) {
			return;
		}
		try {
			handlerMethod.method.invoke(handlerMethod.instance, p,packet);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void addhandler(Object object) {
		for(Method method : object.getClass().getDeclaredMethods()) {
			PacketListener value = method.getAnnotation(PacketListener.class);
			if(value != null) {
				handlers.put(value.packet(), new HandlerMethod(object, method));
			}
		}
	}
	
	
	@AllArgsConstructor
	private static class HandlerMethod{
		Object instance;
		Method method;
	}
}
